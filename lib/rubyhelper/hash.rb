module HashHelper

  # Return a hash that includes everything but the given keys.
  #
  # @return [Hash]
  def except(*keys)
    return self.dup.except!(*keys)
  end

  # Replaces the hash without the given keys.
  #
  # @return [Hash]
  def except!(*keys)
    keys.each { |key| self.delete(key) }
    return self
  end

  # Rename the keys of a Hash like '{old_ney => new_key}'
  #
  # @param keys [Hash]
  # @return [Hash]
  def rekeys keys={}
    raise ArgumentError, 'keys must be a Hash' unless keys.is_a? Hash
    d = self.dup
    keys.each_key{|old_key| d.delete(old_key)}
    return d.tap{keys.each{|old_key, new_key| d[new_key] = self[old_key]}}
  end

  # see {Hash#rekeys}
  def rekeys!
    return self.replace(self.rekeys(keys))
  end

end

class Hash
  prepend HashHelper
end
