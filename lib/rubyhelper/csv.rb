#encoding: utf-8

module CSVHelper

  # the function split in multiple csv the current data
  #
  # @param names a list of with every csv you want to create. Every will have the same account of data (except the last) and will be take in the order
  # @return the number of files created
  def split(*names)
    return if names.empty? # if no file, no split
    headers = self.header_row? # check if headers exists
    csv = self.to_a # save the entiere csv
    headers = headers ? csv.first.to_h.keys : nil # if headers exists, save it
    csv_size = csv.size # save the csv size
    chunk_size = csv_size / names.size
    chunk_size = 1 if chunk_size.zero? # min size is 1

    # keep only the NLINES first chunks
    names[0..csv_size].each_with_index do |name, i|
      CSV.open(name, 'wb') do |f|
        f << headers if headers
        csv[(i*chunk_size)..((i+1)*chunk_size-1)].map(&:fields).each{|l| f << l}
      end
    end
    return [names.size, csv_size].min
  end

  # see #{split}.
  #
  # @param n [Fixnum] the number of subcsv
  # @param basename [String] the base of each files. will be N_basename
  # @param path [String] the directory to create the files
  def split_in_parts(n=1, basename='temporary.csv', path='/tmp')
    names = 1.upto(n).map do |i|
      Pathname.new(path) + (i.to_s + '_' + basename)
    end
    return split(*names)
  end

end

class CSV
  prepend CSVHelper
end
